#include <vector>
#include <bits/stdc++.h>
using namespace std;

void display_misses(int misses);
void display_status(std::vector<char> incorrect, std::string answer);
void greet();
void end_game(string answer, string codeword);
